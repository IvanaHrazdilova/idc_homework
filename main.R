rm(list = ls())

# This is 

library(dplyr)
library(tidyverse)
library(network)
library(lubridate)
library(plotly)


dat <- read.csv(file = 'openaccess/MetObjects.csv', stringsAsFactors = FALSE)

glimpse(dat)

dat[dat==""]<-NA

# UNDERSTAND TABLE STRUCTURE
# test <- dat %>% group_by(Object.Number) %>% summarise(n = n()) %>% filter(n>1) 
# Object.Number isn't unique
test <- dat %>% group_by(Object.ID) %>% summarise(n = n())
# Object.Number is unique


# SUBSET DATA TO WORK WITH
test2 <- dat %>% filter(Is.Timeline.Work == 'True')
test2 <- dat %>% group_by(Medium) %>% summarise(n = n()) %>% filter(n>1)
test2 <- dat %>% filter(Classification != '') %>% group_by(Classification) %>% summarise(n = n()) %>% arrange(n)
# Classification = Paintings 8539 counts


test3 <- dat %>% filter(Classification == 'Paintings') %>% group_by(Culture) %>% summarise(n = n())

test5 <- dat %>% filter(Classification == 'Paintings') %>% filter(Period != '')

# D3JS dataset
paingtings_sample <- dat %>% filter(Classification == 'Paintings') %>% 
  select(Artist.Display.Name,Artist.Nationality, Artist.Begin.Date, Artist.End.Date, Object.ID) %>% 
  filter(!is.na(Artist.Display.Name)) %>% 
  filter(sapply(strsplit(Artist.Display.Name, "\\|"), 
                       function(x) length(x)) == 1) %>% # remove colaboration artworks
  filter(Artist.Display.Name != 'Unidentified Artist') %>% 
  mutate(Artist.Nationality_clean = sapply(strsplit(Artist.Nationality, ", "), 
                                           function(x) x[1])) %>% # remove eg. "American, born XY"
  na.omit() %>% # remove row with NA in any of columns
  group_by(Artist.Display.Name) %>% 
  mutate(artwork_cnt = n_distinct(Object.ID)) %>%
  filter(artwork_cnt >= 4) %>% ungroup() %>% select(-artwork_cnt)





# HISTOGRAM - ARTWORKS COUNT PER ARTIST each country ?
paingtings_hist <- paingtings_sample %>% 
  group_by(Artist.Display.Name, Artist.Nationality_clean) %>% 
  summarise(Unique.Artworks = n_distinct(Object.ID)) %>% 
  ggplot( aes(x=Unique.Artworks, fill= Artist.Nationality_clean )) +
  geom_histogram(binwidth=5, position = 'dodge' )+
  facet_grid(. ~ Artist.Nationality_clean) +
  # geom_density(alpha=0.6) +
  ggtitle("Count of unique artworks per artist") +
  xlim(1,50) + theme(legend.position="top")
paingtings_hist



# # HISTOGRAM - begin day - mam dost variability ?
# 
# # kolik mam nodes v datasetu ? -swith to continuous whops
# nodes <- paingtings_sample %>% distinct(Artist.Display.Name)
#   
# paingtings_sample_hist_year <- paingtings_sample %>% 
#   group_by(Artist.Begin.Date) %>% 
#   summarise(count = n()) %>% mutate(test = lubridate::year(as.Date(Artist.Begin.Date, format = '%Y'))) %>% 
#   ggplot( aes(x=Artist.Begin.Date, y =  count)) +
#   geom_bar(stat = "identity" ) + 
#   theme(axis.text.x = element_text(angle = 40, vjust = 0.5, hjust=1)) +
#   coord_flip()
# paingtings_sample_hist_year
# # Turn it interactive with ggplotly
# p <- ggplotly(paingtings_sample_hist_year, length = 200)
# p



##################


hist_cut <- dat %>% filter(Classification == 'Paintings') %>% 
  select(Artist.Display.Name,Object.ID) %>% 
  filter(!is.na(Artist.Display.Name)) %>% 
  filter(sapply(strsplit(Artist.Display.Name, "\\|"), 
                function(x) length(x)) == 1) %>% # remove colaboration artworks
  filter(Artist.Display.Name != 'Unidentified Artist') %>%
  group_by(Artist.Display.Name) %>% 
  summarise(Unique.Artworks = n_distinct(Object.ID)) %>%
  na.omit()


max(hist_cut$Unique.Artworks) # 344
quantile(hist_cut$Unique.Artworks, 0.95) # 4 (90% hodnot je <= 4, to je dost ocas lidi co toho moc nenamalovali)
outsiders <- hist_cut %>% filter(Unique.Artworks < 7) # tyhle odstranim - vizualni check ze ani jednoho z nich neznam
hist_cut %>% filter(Unique.Artworks == 7) %>% summarise(n = n()) # yintercept 130


# HISTOGRAM hist_cut
hist_cut_p <- hist_cut %>% 
  ggplot( aes(x=Unique.Artworks)) +
  geom_histogram(binwidth=1, position = 'dodge')+
  ggtitle("Count of unique paintings per artist histogram") +
  # theme_bw() +
  geom_hline(yintercept = 27, lty=2) +
  xlim(1,50) +
  scale_color_brewer(palette = "Set3")+
  theme(legend.position="top")
hist_cut_p

# po odstraneni
hist_cut_p <- hist_cut %>% filter(Unique.Artworks >= 7) %>% 
  ggplot( aes(x=Unique.Artworks)) +
  geom_histogram(binwidth=1, position = 'dodge', fill="salmon")+
  ggtitle("Count of unique artworks per artist") +
  theme_bw() +
  # xlim(1,50) +
  theme(legend.position="top")
hist_cut_p

# D3JS dataset 
out <- dat %>% filter(Classification == 'Paintings') %>% 
  select(Artist.Display.Name,Artist.Nationality, Artist.Begin.Date, Artist.End.Date, Object.ID) %>% 
  filter(!is.na(Artist.Display.Name)) %>% 
  filter(sapply(strsplit(Artist.Display.Name, "\\|"), 
                function(x) length(x)) == 1) %>% # remove colaboration artworks
  filter(Artist.Display.Name != 'Unidentified Artist') %>% 
  mutate(Artist.Nationality_clean = sapply(strsplit(Artist.Nationality, ", "), 
                                           function(x) x[1])) %>% # remove eg. "American, born XY"
  na.omit() %>% # remove row with NA in any of columns
  group_by(Artist.Display.Name) %>% 
  mutate(artwork_cnt = n_distinct(Object.ID)) %>%
  filter(artwork_cnt >= 7) %>% select(-artwork_cnt) %>% distinct()

write.csv(out, "paingtings_sample.csv")


